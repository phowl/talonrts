const express = require('express')
const path = require('path')

const app = express()
app.use(express.static(path.join(__dirname, 'static')))
global.appRoot = path.resolve(__dirname)

const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const expresshandlebars = require('express-handlebars')
const hbs = expresshandlebars.create({
  defaultLayout: 'default',
  extname: '.hbs',
  partialsDir: ['views/partials']
})
app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')

const router = require('./router')
app.use(router)

const port = process.env.PORT || '8989'
app.set('port', port)

const http = require('http')
const httpServer = http.createServer(app)

httpServer.listen(port, () => {
  console.log('Listening on %d', httpServer.address().port)
})
