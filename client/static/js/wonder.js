var client = mqtt.connect('ws://localhost:8765', {
  username: 'test',
  password: 'test'
})

// var client = mqtt.connect('ws://rts-client-server.us-east-1.elasticbeanstalk.com', {
//   username: 'test',
//   password: 'test'
// })

client.on('connect', function () {
  console.log('Connected: ' + client.id)
  client.subscribe('presence')
  client.subscribe('owl/84100D459C05/status')
})

client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  var msgEl = document.getElementById('msg')
  msgEl.innerHTML = message
})

var btn = document.getElementById('btnPublish')
btn.addEventListener('click', function (ev) {
  client.publish('presence', 'Hello mqtt')
})
