const express = require('express')
const path = require('path')

const app = express()
// app.use(express.static(path.join(__dirname, 'static')))
global.appRoot = path.resolve(__dirname)

const compression = require('compression')
const bodyParser = require('body-parser')
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const router = require('./router')
app.use(router)

const helpers = require('./helpers')
const port = helpers.normalizePort(process.env.PORT || '8765')
app.set('port', port)

const http = require('http')
const httpServer = http.createServer(app)
httpServer.on('error', helpers.onError)
httpServer.listen(port, () => {
  console.log('Listening on %d', httpServer.address().port)
})


const mqttServer = require('./mqaServer')
const websocket = require('websocket-stream')
websocket.createServer({server: httpServer}, mqttServer.handle)