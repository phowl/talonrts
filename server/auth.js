const BasicStrategy = require('passport-http').BasicStrategy

module.exports = function (passport) {
  passport.use('api', new BasicStrategy({ passReqToCallback: true },
    function (req, tokenKey, tokenSecret, done) {
      return done(null, {})
    }))
}
