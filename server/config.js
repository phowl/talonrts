module.exports = {
  sql: {
    shadow: {
      dialect: 'postgres',
      username: process.env.OB_MAIN_DB_USERNAME || 'owlbarn',
      password: process.env.OB_MAIN_DB_PASSWORD || '',
      database: process.env.OB_MAIN_DB_NAME || 'owlbarn_shadow',
      host: process.env.OB_MAIN_DB_HOST || '127.0.0.1',
      port: process.env.OB_MAIN_DB_PORT || '5432',
      logging: false
    }
  },
  redis: {
    mqtt: {
      host: process.env.OB_REDIS_HOST || '127.0.0.1',
      port: process.env.OB_REDIS_PORT || '6379'
    }
  },

  email: {
    outbound: process.env.OB_FROM_EMAIL || 'admin@owllabs.com'
  }
}

