module.exports = function (sequelize, DataTypes) {
  var OwlShadow = sequelize.define('OwlShadow', {
    macAddress: DataTypes.STRING,
    connected: DataTypes.INTEGER,
    meetingState: DataTypes.INTEGER,
    eyesState: DataTypes.INTEGER,
    muteState: DataTypes.INTEGER
  },
  {
    classMethods: {
      associate: function (models) { }
    }
  })
  return OwlShadow
}
