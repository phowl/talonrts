const winston = require('winston')

module.exports = {
  normalizePort: (val) => {
    let port = parseInt(val, 10)
    if (isNaN(port)) {
      // named pipe
      return val
    }
    if (port >= 0) {
      // port number
      return port
    }
    return false
  },

  onError: (error) => {
    if (error.syscall !== 'listen') {
      throw error
    }
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        winston.error('Port requires elevated privileges')
        process.exit(1)
      case 'EADDRINUSE':
        winston.error('Port is already in use')
        process.exit(1)
      default:
        throw error
    }
  }
}
