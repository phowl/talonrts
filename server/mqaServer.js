const config = require('./config')

const aedesPersistenceRedis = require('aedes-persistence-redis')
const mqttPersistence = aedesPersistenceRedis({
  port: config.redis.mqtt.port,
  host: config.redis.mqtt.host,
  maxSessionDelivery: 100, // maximum offline messages deliverable on client CONNECT, default is 1000
})
const mqttServer = require('aedes')({
  persistence: mqttPersistence
})

const db = require('./db')

mqttServer.authenticate = function (client, username, token, callback) {
  const valid = (username === 'test' && token.toString() === 'test')
  if (valid) {
    client.user = username
  }
  callback(null, valid)
}

mqttServer.authorizePublish = function (client, packet, callback) {
  // TODO: implement restrictions on publishing
  callback(null)
}

// mqttServer.authorizeSubscribe = function (client, packet, callback) {
//   // TODO: implement restrictions on subscriptions
//   callback(null, true)
// }

mqttServer.on('client', (client) => {
  handleConnectionChange(client, 1)
})

mqttServer.on('clientDisconnect', (client) => {
  handleConnectionChange(client, 0)
})

mqttServer.on('publish', (packet, client) => {
  console.log('published: ' + packet.topic + ' - ' + packet.payload.toString())
  const topicComponents = packet.topic.split('/')
  if (topicComponents && topicComponents[0] === 'owl') {
    handlePropertyChange(client, packet)
  }
})

module.exports = mqttServer

function handleConnectionChange (client, isConnected) {
  if (client.id && client.id.split('_')[0] === 'meetingowl') {
    const mac = client.id.split('_')[1]
    console.log('Owl Disconnected: ' + client.id)
    updateDeviceShadow(mac, { connected: isConnected })
  } else {
    console.log('Client connected:' + client.id)
  }
}

function handlePropertyChange (client, packet) {
  const topicComponents = packet.topic.split('/')
  switch (topicComponents[2]) {
    case 'status':
      const json = JSON.parse(packet.payload.toString())
      updateDeviceShadow(topicComponents[1], { meetingState: json.meetingState })
      break
  }
}

function updateDeviceShadow (macAddress, properties) {
  let input = { macAddress: macAddress }
  return db.OwlShadow
    .findOne({ where: input })
    .then((shadow) => {
      if (!shadow) {
        const join = Object.assign(input, properties)
        return db.OwlShadow.create(join)
      } else {
        return shadow.update(properties)
      }
    })
}
