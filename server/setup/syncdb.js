var winston = require('winston')
var models = require('../db')

models.sequelize.sync()
  .then(function () {
    winston.info('Successfully synced  DB')
  })
  .catch(function (err) {
    winston.error(err)
  })
